CC = gcc
CFLAGS = -g -O0

SOURCES = $(wildcard *.c)



all: nonogram

nonogram: $(SOURCES)
	$(CC) $(CFLAGS) -o $@ $^


.PHONY : clean
clean:
	/bin/rm -f *.o nonogram
