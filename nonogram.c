/*
 * Now the problem is about file parsing and the XX_data structure's.
 *
 * Gotta sleep, see you tomorrow.
 *
 * Thu Apr 26 05:57:08 CST 2012
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LOAD_STR_BUF_SZ		(1024 * 1024)  /* 1MB */
#define LOAD_DAT_BUF_SZ		(300)	/* 300x300 bitmap max */
			/* each row/col can get 20 up to segments */
#define LOAD_SEG_BUF_SZ		(20)

#define BITX	1	/* occupied */
#define BITO	2	/* nothing */
#define BIT_	0	/* empty */

#define CHANGED		9
#define ERROR		2
#define NOT_CHANGED	3
#define FINISHED	4

struct nono_map {
	int width, height;
	char *bitmap;
	int **x_data, **y_data;
};

struct nono_line {
	char *bits, *solution;
	char *occupied, *empty;
	int *data;
	int length;
	int row, column;
	int changed;
	int num_solutions;
};
struct nono_line_buf {
	struct nono_line *reference;
	char *bits;
	int seg_ptr;
	int pos;
};


/* Returns number scanned */
int scan_data(FILE *f, int ***out_buf)
{
	int scan_succ;
	int num_data = 0;
	int num_seg = 0;
	int *buf[LOAD_DAT_BUF_SZ] = {NULL};


	buf[num_data] = calloc(LOAD_SEG_BUF_SZ, sizeof(int));
	for (;;) {
		int c = fgetc(f);
		int i_buf = 0;

		if (c == '\n') {
			(buf[num_data++])[++num_seg] = -1;
			num_seg = 0;
			ungetc(c = fgetc(f), f);
			buf[num_data] = calloc(LOAD_SEG_BUF_SZ, sizeof(int));
			
			if (c == '-' || c == EOF)
				continue; /* pass loading numbers */
		} else if (c == '.') {
			++num_seg;
		} else if (c == '-' || c == EOF) {
			int **out = calloc(num_data + 1, sizeof(*out));
			buf[num_data][0] = -1;

			memcpy((void *)out, (void *)buf,
					(num_data + 1) * sizeof(int));
			*out_buf = out;
			if (c == '-')
				fgetc(f);
			return num_data;
		} else {
			ungetc(c, f);
		}

		if (fscanf(f, "%d", &i_buf)) {
			(buf[num_data])[num_seg] = i_buf;
		} else {
			perror("load data");
			exit(0);
		}

		if (num_seg >= LOAD_SEG_BUF_SZ ||
		    num_data >= LOAD_DAT_BUF_SZ) {
			perror("buffer overflow");
			exit(0);
		}
	}
}

int
take_bit(struct nono_map *map, int x, int y)
{
	if (x < 0 || x >= map->width || y < 0 || y >= map->height)
		return -1;
	return map->bitmap[x + (y * map->width)];
}
void
write_bit(struct nono_map *map, int x, int y, char new_val)
{
	if (x < 0 || x >= map->width || y < 0 || y >= map->height)
		return;
	map->bitmap[x + (y * map->width)] = new_val;
}


struct nono_map *
load_data(const char *filename)
{
	FILE *f;
	char *str_buf;
	struct nono_map *map;

	if (!(f = fopen(filename, "r"))) {
		perror("file can't be opened");
		exit(0);
	}

	str_buf = calloc(LOAD_STR_BUF_SZ, sizeof(*str_buf));
	map = calloc(1, sizeof(*map));

	map->width = scan_data(f, &(map->y_data));
	map->height = scan_data(f, &(map->x_data));

	map->bitmap = calloc(map->height * map->width, sizeof(*map->bitmap));

	return map;
}

struct nono_line *
take_line_row(struct nono_map *map, int ypos)
{
	struct nono_line *line;
	int i;

	if (ypos < 0 || ypos >= map->height) {
		return NULL;
	}


	line = calloc(1, sizeof(*line));

	line->data = map->x_data[ypos];
	line->length = map->width;
	line->row = ypos;
	line->column = -1;
	line->changed = NOT_CHANGED;
	line->num_solutions = 0;

	line->bits = calloc(line->length, sizeof(*line->bits));
	line->occupied = calloc(line->length, sizeof(*line->bits));
	line->empty = calloc(line->length, sizeof(*line->bits));
	line->solution = calloc(line->length, sizeof(*line->bits));

	for (i = 0; i != map->width; ++i)
		line->bits[i] = take_bit(map, i, ypos);
	memset(line->occupied, BITX, line->length);
	memset(line->empty, BITO, line->length);

	return line;
}

struct nono_line *
take_line_col(struct nono_map *map, int xpos)
{
	struct nono_line *line;
	int i;

	if (xpos < 0 || xpos >= map->width)
		return NULL;

	line = calloc(1, sizeof(*line));

	line->data = map->y_data[xpos];
	line->length = map->height;
	line->row = -1;
	line->column = xpos;
	line->changed = NOT_CHANGED;
	line->num_solutions = 0;


	line->bits = calloc(line->length, sizeof(*line->bits));
	line->solution = calloc(line->length, sizeof(*line->bits));
	line->occupied = calloc(line->length, sizeof(*line->bits));
	line->empty = calloc(line->length, sizeof(*line->bits));

	for (i = 0; i != map->height; ++i)
		line->bits[i] = take_bit(map, xpos, i);
	memset(line->occupied, BITX, line->length);
	memset(line->empty, BITO, line->length);

	return line;
}

struct nono_line_buf *
make_line_buf(struct nono_line *line)
{
	struct nono_line_buf *buf;

	if (!line)
		return NULL;

	buf = calloc(1, sizeof(*buf));

	buf->reference = line;
	buf->bits = calloc(line->length, sizeof(*buf->bits));
	buf->seg_ptr = 0;

	return buf;
}

void
dbg_print_bits(const char *bits, int length)
{
	int i;
	for (i = 0; i != length; ++i) {
		fprintf(stderr, "%c", bits[i] == 0 ? '-' :
				bits[i] == 1 ? 'X' :
				bits[i] == 2 ? 'O' : '?');
	}
}
void
dbg_print_buf(struct nono_line_buf *buf, const char *text)
{
	fprintf(stderr, "%s: ", text);
	dbg_print_bits(buf->bits, buf->reference->length);
	fprintf(stderr, "\n");
}

void
dbg_print_data(struct nono_line *line, const char *text)
{
	fprintf(stderr, "%s (", text);
	int *p = line->data;
	while (*++p != -1) {
		fprintf(stderr, "%d, ", p[-1]);
	}
	fprintf(stderr, "%d) at %p\n", p[-1], line->data);
}

void
free_line(struct nono_line *line)
{
	if (!line)
		return;
	if (line->bits)
		free(line->bits);
	free(line);
}
void
free_line_buf(struct nono_line_buf *line_buf)
{
	if (!line_buf)
		return;
	if (line_buf->bits)
		free(line_buf->bits);
	free(line_buf);
}

struct nono_line_buf *
line_buf_dup(struct nono_line_buf *line_buf)
{
	struct nono_line_buf *new_buf =
		make_line_buf(line_buf->reference);
	memcpy(new_buf->bits, line_buf->bits,
			line_buf->reference->length);
	new_buf->pos = line_buf->pos;
	new_buf->seg_ptr = line_buf->seg_ptr;

	return new_buf;
}

int
test_finished(struct nono_map *map)
{
	int i, j;
	for (i = 0; i != map->width; ++i) {
		for (j = 0; j != map->height; ++j) {
			if (take_bit(map, i, j) == BIT_)
				return 0;
		}
	}
	return 1;
}

int
fill_buf(struct nono_line_buf *buf, int length, char what)
{
	if (buf->pos + length < buf->reference->length + 1) {
		memset(buf->bits + buf->pos, what, length);
		buf->pos += length;
		return 0;
	}
	/* dbg_print_buf(buf, "buffer overflow"); */
	return 1;
}

void
integrate(struct nono_line_buf *buf)
{
	struct nono_line *line = buf->reference;
	int i;

	for (i = 0; i != line->length; ++i) {
		if (buf->bits[i] != BITX &&
		    line->occupied[i] == BITX) {
			line->occupied[i] = BIT_;
		}
		if (buf->bits[i] == BITX &&
		    line->empty[i] == BITO) {
			line->empty[i] = BIT_;
		}
	}
}

int
test_buf_valid(struct nono_line_buf *buf)
{
	struct nono_line *line = buf->reference;
	int i;

	for (i = 0; i != line->length; ++i) {
		if (line->bits[i] == BITX && buf->bits[i] != BITX) {
			return 0;
		} else if (line->bits[i] == BITO &&
				buf->bits[i] == BITX) {
			return 0;
		}
		line->solution[i] = buf->bits[i];
	}

	++line->num_solutions;
	return 1;
}

void
fill_line(struct nono_line_buf *buf, struct nono_line *line)
{
	int test_space = buf->seg_ptr == 0 ? 0 : 1;
	
	if (buf->reference->data[buf->seg_ptr] == -1) {
		if (test_buf_valid(buf)) {
			integrate(buf);
		}

		return;
	}

	while (buf->pos-1 + test_space + buf->reference->data[buf->seg_ptr]
			< buf->reference->length) {
		struct nono_line_buf *new_buf = line_buf_dup(buf);

		new_buf->seg_ptr++;
		fill_buf(new_buf, test_space, BITO);
		fill_buf(new_buf, buf->reference->data[buf->seg_ptr], BITX);

		fill_line(new_buf, line);

		free_line_buf(new_buf);

		++test_space;
	}
}

int
line_check_changed(struct nono_line *line)
{
	int i;
	for (i = 0; i != line->length; ++i) {
		if (line->bits[i] == BIT_ &&
		    (line->occupied[i] == BITX || line->empty[i] == BITO))
			return 1;
	}
	return 0;
}

void
merge(struct nono_map *map, struct nono_line *line)
{
	int i;
	for (i = 0; i != line->length; ++i) {
		if (line->occupied[i] == BITX) {
			write_bit(map,
				line->column == -1 ? i : line->column,
				line->row == -1 ? i : line->row,
				BITX);
		}
		if (line->empty[i] == BITO) {
			write_bit(map,
				line->column == -1 ? i : line->column,
				line->row == -1 ? i : line->row,
				BITO);
		}
	}
}

void
merge_solution(struct nono_map *map, struct nono_line *line)
{
	int i;
	for (i = 0; i != line->length; ++i) {
		write_bit(map,
			line->column == -1 ? i : line->column,
			line->row == -1 ? i : line->row,
			line->solution[i]);
	}
}

void
solve_line(struct nono_map *map, struct nono_line *line)
{
	struct nono_line_buf *buf = make_line_buf(line);

	/* fprintf(stderr, "a new line is going to be solved.\n"); */
	/* dbg_print_data(line, "data is"); */

	fill_line(buf, line);

	if (line->num_solutions == 1) {
		merge_solution(map, line);
		line->changed = CHANGED;
	} else if (line->num_solutions == 0) {
		line->changed = ERROR;
		return;
	}

	if (line_check_changed(line)) {
		line->changed = CHANGED;
		merge(map, line);
	}
}

void
output(struct nono_map *map)
{
	int i, j;
	for (i = 0; i != map->width; ++i) {
		for (j = 0; j != map->height; ++j) {
			if (take_bit(map, i, j) == BITX)
				printf("*");
			else
				printf("-");
		}
		printf("\n");
	}
}



int
recursion_solve(struct nono_map *map)
{
	int col, row;
	int state = NOT_CHANGED;

	if (test_finished(map)) {
		return FINISHED;
	}

	for (col = 0; col != map->width; ++col) {
		struct nono_line *line = take_line_col(map, col);
		
		solve_line(map, line);
		if (line->changed == CHANGED) {
			state = CHANGED;
		} else if (line->changed == ERROR) {
			return ERROR;
		}
		free_line(line);
	}
	for (row = 0; row != map->height; ++row) {
		struct nono_line *line = take_line_row(map, row);
		solve_line(map, line);
		if (line->changed) {
			state = CHANGED;
		} else if (line->changed == ERROR) {
			return ERROR;
		}
		free_line(line);
	}

	printf("----------------------------\n");
	output(map);
	printf("----------------------------\n");
	
	if (state == CHANGED)
		return CHANGED;
	else
		return NOT_CHANGED;
}

int
solve(struct nono_map *map)
{
	int result;

	if (!map) {
		return -3;
	}

	for (;;) {
		result = recursion_solve(map);
		if (result == CHANGED) {
			continue;
		} else if (result == ERROR) {
			return -2; /* Error */
		} else if (result == NOT_CHANGED) {
			return -1;
		} else if (result == FINISHED) {
			return 0;
		} else {
			/* impossible to run to there */
		}
	}

	return -4;
}

int main(int argc, char **argv)
{
	struct nono_map *map = NULL;
	if (argc == 2) {
		map = load_data(argv[1]);
	} else {
		printf("Usage %s nonogram.nono\n", argv[0]);
		exit(0);
	}

	solve(map);

	output(map);

	return 0;
}


